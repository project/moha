<?php
/**
 * @file
 */
?>

<label for="name">Name: </label>
<div class="input_box_wrapper">
  <input id="name" title="" type="text" class="gradient_border_text_input" autocomplete="off"/>
</div>

<label for="gender">Gender: </label>
<div class="input_box_wrapper_round">
  <input id="gender" title="" type="text" class="gradient_border_text_input"/>
</div>


